# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.5.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
import plotly.graph_objects as go
import ipywidgets as widgets
from ipywidgets import interact, interact_manual
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
import numpy as np
import os
import math
import feature_selection
from sklearn.model_selection import cross_validate
from sklearn.model_selection import KFold
from sklearn.model_selection import ShuffleSplit
import matplotlib.pylab as plt
import matplotlib as mpl
from sklearn.metrics import mean_absolute_error
import xlsxwriter
from xlsxwriter.utility import xl_range

mpl.rcParams['figure.figsize'] = (40,10)
mpl.rcParams['axes.titlesize'] = 26
mpl.rcParams['legend.fontsize'] = 26
mpl.rcParams['axes.labelsize'] = 26
mpl.rcParams['axes.labelweight'] = 'bold'
mpl.rcParams['xtick.labelsize'] = 20
mpl.rcParams['ytick.labelsize'] = 20
mpl.rcParams['lines.linewidth'] = 5
mpl.rcParams['axes.facecolor'] = '.9'
mpl.rcParams['grid.linestyle'] = '--'
mpl.rcParams['grid.linewidth'] = 2
mpl.rcParams['lines.markersize'] =  12.0
mpl.rc('figure', max_open_warning = 0)
plt.style.use('seaborn-darkgrid')

path_to_raw_files = os.path.join(os.path.expanduser("~")+os.sep,"Clariant International Ltd\Digital methods in R&D and Engineering Services - Pilots","AI 4 Material Characterization","Alumina - predict tabletability")

#raw_data_02 = pd.read_excel(os.path.join(path_to_raw_files,'AI_4_Material_Alumina_consolidated_data.xlsx'))
raw_data = pd.read_excel('AI_4_Alumina_Prepared_Data.xlsx')

data_boehmite = raw_data[(raw_data['Type']=='pure_boehmite') |(raw_data['Type']=='boehmite_with_binder') ].copy()
pure_boehmite = raw_data[raw_data['Type']=='pure_boehmite'].copy()
data_with_binder = data_boehmite[(data_boehmite['Type']=='boehmite_with_binder') ].copy()
data_tabletted = raw_data[(raw_data['is_tabletted']=='Yes')].copy()
data_with_binder_tabletted = raw_data[((raw_data['Type']=='boehmite_with_binder')) & (raw_data['is_tabletted']=='Yes')].copy()

# +
all_binders = data_boehmite[(data_boehmite['Type']=='boehmite_with_binder') | (data_boehmite['Type']=='boehmite_boehmite_with_binder_sieve_fractionwith_binder') ].copy()
binder_names = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','WDB','SB']
len_frame = pd.DataFrame(index=binder_names, columns=[0])
for i, var in enumerate(binder_names):
    len_frame.loc[var,0] = len(all_binders[all_binders[var]>0])
    print(var+' is in s amples: ',len(all_binders[all_binders[var]>0]))
len_frame = len_frame.sort_values(0,ascending=False)
len_all_binders = len(all_binders)

fig1, ax1 = plt.subplots()
ax1.bar(['All samples'],len(raw_data))
ax1.text(['All samples'],len(raw_data)+0.25,len(raw_data),fontweight='bold',fontsize=20)
ax1.bar(['All samples with binders'],len_all_binders)
ax1.text(['All samples with binders'],len_all_binders+0.25,len_all_binders,fontweight='bold',fontsize=20)
for var in len_frame.index:
    ax1.bar(var,len_frame.loc[var,0])
    ax1.text(var,len_frame.loc[var,0]+0.25,len_frame.loc[var,0],fontweight='bold',fontsize=20)
ax1.set_title('Number of samples with at least 1 binder / additional boehmite')
fig1.tight_layout()
fig1.savefig(os.path.join('output_figures','number_samples_at_least.jpg'))    
    
fig1, ax1 = plt.subplots()
ax1.bar(['All samples'],len(raw_data))
ax1.text(['All samples'],len(raw_data)+0.25,len(raw_data),fontweight='bold',fontsize=20)
ax1.bar(['All samples with binders'],len_all_binders)
ax1.text(['All samples with binders'],len_all_binders+0.25,len_all_binders,fontweight='bold',fontsize=20)
ax1.bar('graphite',len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']>0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]))
ax1.text(['graphite'],len(all_binders[(all_binders['Al_stearate']>0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)])+0.25,len(all_binders[(all_binders['Al_stearate']>0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]),fontweight='bold',fontsize=20)
ax1.bar('Al_stearate',len(all_binders[(all_binders['Al_stearate']>0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]))
ax1.text(['Al_stearate'],len(all_binders[(all_binders['Al_stearate']>0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)])+0.25,len(all_binders[(all_binders['Al_stearate']>0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]),fontweight='bold',fontsize=20)
ax1.bar('perlite',len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']>0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]))
ax1.text(['perlite'],len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']>0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)])+0.25,len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']>0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]),fontweight='bold',fontsize=20)
ax1.bar('Mg_Stearate',len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']>0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]))
ax1.text(['Mg_Stearate'],len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']>0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)])+0.25,len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']>0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]),fontweight='bold',fontsize=20)
ax1.bar('Gurh',len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']>0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]))
ax1.text(['Gurh'],len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']>0)&(all_binders['WDB']==0)&(all_binders['SB']==0)])+0.25,len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']>0)&(all_binders['WDB']==0)&(all_binders['SB']==0)]),fontweight='bold',fontsize=20)
ax1.bar('WDB',len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']>0)&(all_binders['SB']==0)]))
ax1.text(['WDB'],len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']>0)&(all_binders['SB']==0)])+0.25,len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']>0)&(all_binders['SB']==0)]),fontweight='bold',fontsize=20)
ax1.bar('SB',len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']>0)]))
ax1.text(['SB'],len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']>0)])+0.25,len(all_binders[(all_binders['Al_stearate']==0) &(all_binders['Mg_Stearate']==0)&(all_binders['graphite']==0)&(all_binders['perlite']==0)
                &(all_binders['Gurh']==0)&(all_binders['WDB']==0)&(all_binders['SB']>0)]),fontweight='bold',fontsize=20)
ax1.set_title('Number of samples with exactly 1 binder / additional boehmite')
fig1.tight_layout()
fig1.savefig(os.path.join('output_figures','number_samples_exactly.jpg'))    


# -

def generate_list_high_missing_values(data_frame,missing_ratio):
    return list(data_frame.isna().sum().index[data_frame.isna().sum()/data_frame.shape[0]>missing_ratio])


def evaluation_function_for_screening(featureset, **kwargs):
    
    source_frame = kwargs['data_frame'].copy()
    target = kwargs['target_variable']
    input_var = list(featureset)
    data_nona = source_frame.dropna(subset=input_var+[target]).copy()
    X = data_nona[input_var]
    y = data_nona[[target]]
    reg = LinearRegression(fit_intercept=False).fit(X, y)
    y_predict = reg.predict(X).reshape(1,len(y))[0]
    y_true = y.values.reshape(1,len(y))[0]
    return mean_squared_error(y_true, y_predict)


def screen_linear_model(target, must_features, can_features, max_features, data_frame, missing_ratio, add_feature):
    forward_selector_func = feature_selection.FeatureSelector()
    forward_selector_func.forward_selector(must_features=must_feature,can_features=can_features,evaluationfunction=evaluation_function_for_screening, better='lower',max_features=max_features,data_frame=data_frame, target_variable=target)
    input_var = must_features+forward_selector_func.best_set()
    data_nona = data_frame.dropna(subset=input_var+[target]).copy()
    X = data_nona[input_var]
    y = data_nona[[target]]
    number_splits = 5
    reg = LinearRegression(fit_intercept=False).fit(X, y)
    kf = KFold(n_splits=number_splits, shuffle=True)
    fig1, ax1 = plt.subplots(figsize=(40,10))
    ax1.plot(data_nona['Sample_Name'],data_nona[target], color='black', label='Measured Values')
    i = 0
    test_error_sum = []
    train_error_sum = []
    total_error_sum = []
    coefficient_matrix = pd.DataFrame(index=range(number_splits), columns=input_var, data=np.nan)
    for train_index, test_index in kf.split(X):
        X_train, X_test = X.loc[X.index[train_index],:], X.loc[X.index[test_index],:]
        y_train, y_test = y.loc[y.index[train_index],:], y.loc[y.index[test_index],:]
        reg_cv = LinearRegression(fit_intercept=False).fit(X_train, y_train)
        coefficient_matrix.loc[i,:] = reg_cv.coef_
        y_predict_test_cv = reg_cv.predict(X_test).reshape(1,len(X_test))[0]
        y_true_test_cv = y_test.values.reshape(1,len(y_test))[0]
        y_predict_train_cv = reg_cv.predict(X_train).reshape(1,len(X_train))[0]
        y_true_train_cv = y_train.values.reshape(1,len(y_train))[0]
        y_predict_total_cv = reg_cv.predict(X).reshape(1,len(X))[0]
        y_true_total_cv = y.values.reshape(1,len(y))[0]
        test_error = mean_squared_error(y_true_test_cv, y_predict_test_cv)
        train_error = mean_squared_error(y_true_train_cv, y_predict_train_cv)
        total_error = mean_squared_error(y_true_total_cv, y_predict_total_cv)
        test_error_sum.append(test_error)
        train_error_sum.append(train_error)
        total_error_sum.append(total_error)
        min_value = min(min(y_true_train_cv),min(y_predict_train_cv),min(y_predict_test_cv),min(y_true_test_cv))
        max_value = max(max(y_true_train_cv),max(y_predict_train_cv),max(y_predict_test_cv),max(y_true_test_cv))        
        ax1.scatter(data_nona.loc[data_nona.index[train_index],'Sample_Name'],y_predict_train_cv, color = 'red', label='Training Data Split '+str(i+1))
        ax1.scatter(data_nona.loc[data_nona.index[test_index],'Sample_Name'],y_predict_test_cv, color = 'blue', label='Test Data Split '+str(i+1))
        i = i+1
    ax1.legend()
    ax1.set_ylabel(target)
    ax1.set_title('Comparison of Training/Test Results for different splits from CV')    
    plt.setp(ax1.get_xticklabels(), rotation=45, ha='right')
    fig1.tight_layout()
    fig1.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_compare_training_test_with_measured.jpg'))
    y_predict = reg.predict(X).reshape(1,len(y))[0]
    y_true = y.values.reshape(1,len(y))[0]
    total_MAPE = np.mean(np.abs((y_true - y_predict) / y_true)) * 100
    
    fig3, ax3 = plt.subplots(figsize=(20,20))
    ax3.bar(['Mean Train Error','Mean Test Error','Mean Total Error'],[np.mean(train_error_sum),np.mean(test_error_sum),np.mean(total_error_sum)])
    ax3.set_ylabel('Mean Squared Error')
    ax3.set_title('Comparison of Mean Training, Test and Total Model Error from CV')
    fig3.tight_layout()
    fig3.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_mean_train_test_cv_error.jpg'))
    
    fig3, ax3 = plt.subplots(figsize=(20,20))
    ax3.bar(['Number of Total Samples','Number of Samples in Model'],[len(data_frame), len(data_nona)])
    ax3.set_ylabel('Number of Samples')
    ax3.set_title('Comparison of available Samples of Total Set and Set for Modelling')
    fig3.tight_layout()
    fig3.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_number_samples.jpg'))
    data_nona['y_predicted'] = y_predict
    data_nona['increased_true'] = np.sign(data_nona[target] -data_nona['Pure_'+target] )
    data_nona['increased_predicted'] = np.sign(data_nona['y_predicted'] -data_nona['Pure_'+target])
    increase_correct = np.shape(np.where((data_nona['increased_true']==1) & (data_nona['increased_predicted']==1)))[1]
    decrease_correct = np.shape(np.where((data_nona['increased_true']==-1) & (data_nona['increased_predicted']==-1)))[1]
    reg.coef_ = reg.coef_[0]
    ols_formula = target + ' = %0.4f +' % reg.intercept_
    for i, col in enumerate(X.columns[0:]):
        if np.sign(reg.coef_[i])>=0:
            ols_formula += ' %0.4f * %s +' % (reg.coef_[i], col)
        if np.sign(reg.coef_[i])<0:
            ols_formula += ' %0.4f * %s -' % (abs(reg.coef_[i]), col)
    print(' '.join(ols_formula.split(' ')[:-1]))
    print('MSE: ', round(mean_squared_error(y_true, y_predict),4))
    
    min_value = min(min(y_true),min(y_predict))
    max_value = max(max(y_true),max(y_predict))
    
    fig1, ax1 = plt.subplots(figsize=(40,10))
    ax1.scatter(data_nona['Sample_Name'],data_nona[target], color='green', label='Measured Values')
    ax1.plot(data_nona['Sample_Name'],data_nona['y_predicted'], color='black', label='Predicted Values')
    ax1.set_ylabel(target)
    ax1.legend()
    ax1.set_title('Results of simple linear model for prediction of '+target)
    plt.setp(ax1.get_xticklabels(), rotation=45, ha='right')
    fig1.tight_layout()
    fig1.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_compare_modelling_with_measured.jpg'))
    plt.show()
    
    return len(data_frame), len(data_nona), input_var, reg.coef_,np.mean(train_error_sum), np.mean(test_error_sum),np.mean(total_error_sum), mean_squared_error(y_true, y_predict), total_MAPE, (increase_correct+decrease_correct)/len(data_nona)*100, coefficient_matrix


def screen_linear_model_tablet(target, must_features, can_features, max_features, data_frame, missing_ratio, add_feature):
    forward_selector_func = feature_selection.FeatureSelector()
    forward_selector_func.forward_selector(must_features=must_feature,can_features=can_features,evaluationfunction=evaluation_function_for_screening, better='lower',max_features=max_features,data_frame=data_frame, target_variable=target)
    input_var = must_features+forward_selector_func.best_set()
    data_nona = data_frame.dropna(subset=input_var+[target]).copy()
    X = data_nona[input_var]
    y = data_nona[[target]]
    number_splits = 5
    reg = LinearRegression(fit_intercept=False).fit(X, y)
    kf = KFold(n_splits=number_splits, shuffle=True)
    fig1, ax1 = plt.subplots(figsize=(40,10))
    ax1.plot(data_nona['Sample_Name'],data_nona[target], color='black', label='Measured Values')
    i = 0
    test_error_sum = []
    train_error_sum = []
    total_error_sum = []
    coefficient_matrix = pd.DataFrame(index=range(number_splits), columns=input_var, data=np.nan)
    for train_index, test_index in kf.split(X):
        X_train, X_test = X.loc[X.index[train_index],:], X.loc[X.index[test_index],:]
        y_train, y_test = y.loc[y.index[train_index],:], y.loc[y.index[test_index],:]
        reg_cv = LinearRegression(fit_intercept=False).fit(X_train, y_train)
        coefficient_matrix.loc[i,:] = reg_cv.coef_
        y_predict_test_cv = reg_cv.predict(X_test).reshape(1,len(X_test))[0]
        y_true_test_cv = y_test.values.reshape(1,len(y_test))[0]
        y_predict_train_cv = reg_cv.predict(X_train).reshape(1,len(X_train))[0]
        y_true_train_cv = y_train.values.reshape(1,len(y_train))[0]
        y_predict_total_cv = reg_cv.predict(X).reshape(1,len(X))[0]
        y_true_total_cv = y.values.reshape(1,len(y))[0]
        test_error = mean_squared_error(y_true_test_cv, y_predict_test_cv)
        train_error = mean_squared_error(y_true_train_cv, y_predict_train_cv)
        total_error = mean_squared_error(y_true_total_cv, y_predict_total_cv)
        test_error_sum.append(test_error)
        train_error_sum.append(train_error)
        total_error_sum.append(total_error)
        min_value = min(min(y_true_train_cv),min(y_predict_train_cv),min(y_predict_test_cv),min(y_true_test_cv))
        max_value = max(max(y_true_train_cv),max(y_predict_train_cv),max(y_predict_test_cv),max(y_true_test_cv))        
        ax1.scatter(data_nona.loc[data_nona.index[train_index],'Sample_Name'],y_predict_train_cv, color = 'red', label='Training Data Split '+str(i+1))
        ax1.scatter(data_nona.loc[data_nona.index[test_index],'Sample_Name'],y_predict_test_cv, color = 'blue', label='Test Data Split '+str(i+1))
        i = i+1
    ax1.legend()
    ax1.set_ylabel(target)
    ax1.set_title('Comparison of Training/Test Results for different splits from CV')
    plt.setp(ax1.get_xticklabels(), rotation=45, ha='right')
    fig1.tight_layout()
    fig1.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_compare_training_test_with_measured.jpg'))
    y_predict = reg.predict(X).reshape(1,len(y))[0]
    y_true = y.values.reshape(1,len(y))[0]
    total_MAPE = np.mean(np.abs((y_true - y_predict) / y_true)) * 100
    
    fig3, ax3 = plt.subplots(figsize=(20,20))
    ax3.bar(['Mean Train Error','Mean Test Error','Mean Total Error'],[np.mean(train_error_sum),np.mean(test_error_sum),np.mean(total_error_sum)])
    ax3.set_ylabel('Mean Squared Error')
    ax3.set_title('Comparison of Mean Training, Test and Total Model Error from CV')
    fig3.tight_layout()
    fig3.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_mean_train_test_cv_error.jpg'))
    
    fig3, ax3 = plt.subplots(figsize=(20,20))
    ax3.bar(['Number of Total Samples','Number of Samples in Model'],[len(data_frame), len(data_nona)])
    ax3.set_ylabel('Number of Samples')
    ax3.set_title('Comparison of available Samples of Total Set and Set for Modelling')
    fig3.tight_layout()
    fig3.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_number_samples.jpg'))
    data_nona['y_predicted'] = y_predict
    reg.coef_ = reg.coef_[0]
    ols_formula = target + ' = %0.4f +' % reg.intercept_
    for i, col in enumerate(X.columns[0:]):
        if np.sign(reg.coef_[i])>=0:
            ols_formula += ' %0.4f * %s +' % (reg.coef_[i], col)
        if np.sign(reg.coef_[i])<0:
            ols_formula += ' %0.4f * %s -' % (abs(reg.coef_[i]), col)
    print(' '.join(ols_formula.split(' ')[:-1]))
    print('MSE: ', round(mean_squared_error(y_true, y_predict),4))
    
    min_value = min(min(y_true),min(y_predict))
    max_value = max(max(y_true),max(y_predict))
    
    fig1, ax1 = plt.subplots(figsize=(40,10))
    ax1.scatter(data_nona['Sample_Name'],data_nona[target], color='green', label='Measured Values')
    ax1.plot(data_nona['Sample_Name'],data_nona['y_predicted'], color='black', label='Predicted Values')
    ax1.set_ylabel(target)
    ax1.legend()
    ax1.set_title('Results of simple linear model for prediction of '+target)
    plt.setp(ax1.get_xticklabels(), rotation=45, ha='right')
    fig1.tight_layout()
    fig1.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_compare_modelling_with_measured.jpg'))
    plt.show()
    
    return len(data_frame), len(data_nona), input_var, reg.coef_,np.mean(train_error_sum), np.mean(test_error_sum),np.mean(total_error_sum), mean_squared_error(y_true, y_predict), total_MAPE,  coefficient_matrix


source_frame = data_with_binder.copy()
missing_ratio = 0.2
list_high_missing_values = generate_list_high_missing_values(source_frame,missing_ratio)
result_df=pd.DataFrame(index=list([var for var in list(data_with_binder.columns) if 'Flow' in var and 'Pure' not in var]), data=np.nan, columns=['No_Sample_Source','No_Sample_Model',
                                                                                                                                                 'Input_Var','Coefficients','Mean_Train_Error', 'Mean_Test_Error',
                                                                                                                                                 'Mean_Total_Error','Total_Error', 'Total_MAPE', 'Correct_Effect'])
workbook = xlsxwriter.Workbook(os.path.join('output_data','results_flow_properties.xlsx'))
worksheet = workbook.add_worksheet()
bgcolor = workbook.add_format({'bg_color': 'orange'})
counter_var = 0
for add_feature in [0,2,5]:
    for number_of_run, target_var in enumerate([var for var in list(source_frame.columns) if 'Flow' in var and 'Pure' not in var]):
    #for number_of_run, target_var in enumerate(['Flow_FRI']):
        #target = 'Flow_FRI'
        target = target_var
        print('#########   '+target+' with '+str(add_feature)+' additional Features  #########')
        must_feature = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','WDB','SB','Pure_'+target]
        #must_feature = []
        #can_features = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and var not in ['Flow_FRI']]
        can_features = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and 'Pure' in var]
        try:
            i,j,k,l,m,n,o, p,q,r,s = screen_linear_model(target=target,must_features=must_feature,can_features=can_features,max_features=len(must_feature)+add_feature,data_frame=source_frame,missing_ratio=0.2, add_feature=add_feature)
            result_df.loc[target,'No_Sample_Source'] = i
            result_df.loc[target,'No_Sample_Model'] = j
            result_df.loc[target,'Input_Var'] = [np.array(k).reshape(1,len(k))]
            result_df.loc[target,'Coefficients'] = [np.array(k).reshape(1,len(k))]
            result_df.loc[target,'Coefficients'] = [np.array(list(l)).reshape(1,len(l))]
            result_df.loc[target,'Mean_Train_Error'] = m
            result_df.loc[target,'Mean_Test_Error'] = n
            result_df.loc[target,'Mean_Total_Error'] = o
            result_df.loc[target,'Total_Error'] = p
            result_df.loc[target,'Total_MAPE'] = q
            result_df.loc[target,'Correct_Effect'] = r
            
            worksheet.write(counter_var*9+1, 1,target[5:])
            worksheet.write(counter_var*9+1, 2,'with additional '+str(add_feature)+' features')
            for help_i, help_j in enumerate(list([var for var in list(result_df.columns) if 'Input_Var' not in var and 'Coefficients' not in var])):
                worksheet.write(counter_var*9+2, 1+help_i,help_j)
                worksheet.write(counter_var*9+3, 1+help_i,result_df.loc[target,help_j])
            worksheet.write_row(counter_var*9+5, 1,k)
            worksheet.write_row(counter_var*9+6, 1,l)
            worksheet.write(counter_var*9+7, 0,'Input')
            for help_i in range(len(l)):
                worksheet.write(counter_var*9+7,1+help_i,'',bgcolor)
            worksheet.write(counter_var*9+6, 1+len(l),'Result')
            formula_line_1 = xl_range(counter_var*9+6, 1, counter_var*9+6, len(l))
            formula_line_2 = xl_range(counter_var*9+7, 1, counter_var*9+7, len(l))
            formula = 'sumproduct({0},{1})'.format(formula_line_1,formula_line_2)
            worksheet.write_formula(counter_var*9+7,1+len(l),formula)
            counter_var = counter_var + 1
            
            fig1,ax1=plt.subplots(figsize=(40,10))
            ax1.scatter(s.min(axis=0),list(s.columns),marker='o', facecolors='white', edgecolors='black',zorder=2)
            ax1.scatter(s.mean(axis=0),list(s.columns),marker='o',color='black',zorder=2)
            ax1.scatter(s.max(axis=0),list(s.columns),marker='o', facecolors='white', edgecolors='black',zorder=2)
            ax1.vlines(0,-1,len(list(s.columns)), color='black')
            for i in range(len(list(s.columns))):
                ax1.plot([s.loc[:,s.columns[i]].min(),s.loc[:,s.columns[i]].mean(),s.loc[:,s.columns[i]].max()],[s.columns[i],s.columns[i],s.columns[i]], zorder=1, color='black')
            ax1.set_ylim(-0.5,len(list(s.columns))-0.5)
            ax1.set_xlabel('Min/Mean/Max Value of Coefficients from Cross Validation')
            ax1.set_title('Range of Model Coefficients from Splits from CV for '+target)
            fig1.tight_layout()
            fig1.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_compare_coefficients_from_CV.jpg'))
            plt.show()
        except:
            continue
    fig1, ax1=plt.subplots(figsize=(40,10))
    ax1.bar(list(result_df.index),result_df['Total_MAPE'])
    ax1.set_ylabel('Mean Absolute Percentage Error / %')
    ax1.set_title('Mean Absolute Perentage Error for '+str(add_feature)+' additional Features')
    plt.setp(ax1.get_xticklabels(), rotation=25, ha='right')
    fig1.tight_layout()
    fig1.savefig(os.path.join('output_figures',str(add_feature)+'_flow_MAPE.jpg'))
    
    fig1, ax1=plt.subplots(figsize=(40,10))
    ax1.bar(list(result_df.index),result_df['Correct_Effect'])
    ax1.set_ylabel('Percentage of correct Predicitions of Increase/Decrease after binder addition / %')
    ax1.set_title('Percentage of correct Predicitions of Increase/Decrease after binder addition for '+str(add_feature)+' additional Features')
    plt.setp(ax1.get_xticklabels(), rotation=25, ha='right')
    fig1.tight_layout()
    fig1.savefig(os.path.join('output_figures',str(add_feature)+'_flow_percentage_correct.jpg'))
    
    result_df.to_excel(os.path.join('output_data','results_flow_properties_'+str(add_feature)+'.xlsx'))
workbook.close()

# source_frame = data_tabletted.copy()
missing_ratio = 0.2
list_high_missing_values = generate_list_high_missing_values(source_frame,missing_ratio)
result_df=pd.DataFrame(index=list([var for var in list(data_with_binder.columns) if 'Flow' in var and 'Pure' not in var]), data=np.nan, columns=['No_Sample_Source','No_Sample_Model',
                                                                                                                                                 'Input_Var','Coefficients','Mean_Train_Error', 'Mean_Test_Error',
                                                                                                                                                 'Mean_Total_Error','Total_Error', 'Total_MAPE'])
workbook = xlsxwriter.Workbook(os.path.join('output_data','results_tablet_properties.xlsx'))
worksheet = workbook.add_worksheet()
bgcolor = workbook.add_format({'bg_color': 'orange'})
counter_var = 0                                                                                                                                                 
for add_feature in [0,2,5]:
    for number_of_run, target_var in enumerate([var for var in list(source_frame.columns) if 'tablet_product' in var and 'Pure' not in var]):
    #for number_of_run, target_var in enumerate(['tablet_product_attrition']):
        #target = 'Flow_FRI'
        target = target_var
        print('#########   '+target+' with '+str(add_feature)+' additional Features  #########')
        must_feature = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','WDB','SB']
        can_features = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and var not in [target] and 'tablet_product' not in var]
        #try:
        i,j,k,l,m,n,o, p,q,s = screen_linear_model_tablet(target=target,must_features=must_feature,can_features=can_features,max_features=len(must_feature)+add_feature,data_frame=source_frame,missing_ratio=0.2, add_feature=add_feature)
        result_df.loc[target,'No_Sample_Source'] = i
        result_df.loc[target,'No_Sample_Model'] = j
        result_df.loc[target,'Input_Var'] = [np.array(k).reshape(1,len(k))]
        result_df.loc[target,'Coefficients'] = [np.array(k).reshape(1,len(k))]
        result_df.loc[target,'Coefficients'] = [np.array(list(l)).reshape(1,len(l))]
        result_df.loc[target,'Mean_Train_Error'] = m
        result_df.loc[target,'Mean_Test_Error'] = n
        result_df.loc[target,'Mean_Total_Error'] = o
        result_df.loc[target,'Total_Error'] = p
        result_df.loc[target,'Total_MAPE'] = q
        
        worksheet.write(counter_var*9+1, 1,target[15:])
        worksheet.write(counter_var*9+1, 2,'with additional '+str(add_feature)+' features')
        for help_i, help_j in enumerate(list([var for var in list(result_df.columns) if 'Input_Var' not in var and 'Coefficients' not in var])):
            worksheet.write(counter_var*9+2, 1+help_i,help_j)
            worksheet.write(counter_var*9+3, 1+help_i,result_df.loc[target,help_j])
        worksheet.write_row(counter_var*9+5, 1,k)
        worksheet.write_row(counter_var*9+6, 1,l)
        worksheet.write(counter_var*9+7, 0,'Input')
        for help_i in range(len(l)):
            worksheet.write(counter_var*9+7,1+help_i,'',bgcolor)
        worksheet.write(counter_var*9+6, 1+len(l),'Result')
        formula_line_1 = xl_range(counter_var*9+6, 1, counter_var*9+6, len(l))
        formula_line_2 = xl_range(counter_var*9+7, 1, counter_var*9+7, len(l))
        formula = 'sumproduct({0},{1})'.format(formula_line_1,formula_line_2)
        worksheet.write_formula(counter_var*9+7,1+len(l),formula)
        counter_var = counter_var + 1

        fig1,ax1=plt.subplots(figsize=(40,10))
        ax1.scatter(s.min(axis=0),list(s.columns),marker='o', facecolors='white', edgecolors='black',zorder=2)
        ax1.scatter(s.mean(axis=0),list(s.columns),marker='o',color='black',zorder=2)
        ax1.scatter(s.max(axis=0),list(s.columns),marker='o', facecolors='white', edgecolors='black',zorder=2)
        ax1.vlines(0,-1,len(list(s.columns)), color='black')
        for i in range(len(list(s.columns))):
            ax1.plot([s.loc[:,s.columns[i]].min(),s.loc[:,s.columns[i]].mean(),s.loc[:,s.columns[i]].max()],[s.columns[i],s.columns[i],s.columns[i]], zorder=1, color='black')
        ax1.set_ylim(-0.5,len(list(s.columns))-0.5)
        ax1.set_xlabel('Min/Mean/Max Value of Coefficients from Cross Validation')
        ax1.set_title('Range of Model Coefficients from Splits from CV for '+target)
        fig1.tight_layout()
        fig1.savefig(os.path.join('output_figures',target+'_'+str(add_feature)+'_compare_coefficients_from_CV.jpg'))
        plt.show()
    #except:
            #continue
    fig1, ax1=plt.subplots(figsize=(40,10))
    ax1.bar(list(result_df.index),result_df['Total_MAPE'])
    ax1.set_ylabel('Mean Absolute Percentage Error / %')
    ax1.set_title('Mean Absolute Perentage Error for '+str(add_feature)+' additional Features')
    plt.setp(ax1.get_xticklabels(), rotation=25, ha='right')
    fig1.tight_layout()
    fig1.savefig(os.path.join('output_figures',str(add_feature)+'_tablet_MAPE.jpg'))
    
    result_df.to_excel(os.path.join('output_data','results_tablet_properties_'+str(add_feature)+'.xlsx'))
workbook.close()


