# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.5.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
import plotly.graph_objects as go
import ipywidgets as widgets
from ipywidgets import interact, interact_manual
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
import numpy as np
import os
import math
import feature_selection

path_to_raw_files = os.path.join(os.path.expanduser("~")+os.sep,"Clariant International Ltd\Digital methods in R&D and Engineering Services - Pilots","AI 4 Material Characterization","Alumina - predict tabletability")

#raw_data_02 = pd.read_excel(os.path.join(path_to_raw_files,'AI_4_Material_Alumina_consolidated_data.xlsx'))
raw_data = pd.read_excel('AI_4_Alumina_Prepared_Data.xlsx')

data_boehmite = raw_data[(raw_data['Type']=='pure_boehmite') |(raw_data['Type']=='boehmite_with_binder') ]
pure_boehmite = raw_data[raw_data['Type']=='pure_boehmite']
data_with_binder = data_boehmite[(data_boehmite['Type']=='boehmite_with_binder') ]


# # General

# * 5 types of samples: Pure Boehmite, Boehmite with Binder, Binder (e.g. graphite), specific sieve fraction of Boehmite with Binder, undefined (30 clicks, Claco Bayerite)
# * Some samples are tabletted

# # Statistics

count_frame = raw_data.groupby(['Type']).size().sort_values(ascending=False)
fig = go.Figure()
fig.add_trace(go.Bar(x=count_frame.index,y=count_frame.values))
fig.update_layout(title_text='Counts for each type of samples')
fig.show()

count_frame = raw_data.groupby(['is_tabletted']).size().sort_values(ascending=False)
fig = go.Figure()
fig.add_trace(go.Bar(x=count_frame.index,y=count_frame.values))
fig.update_layout(title_text='Counts: Was the sample tabletted?')
fig.show()

# ## Distribution of used binders

binder = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','WDB','SB']
binder_pretty = ['Al Stearate','Mg Stearate','Graphite','Perlite','Gurh','WDB','SB']
counter = []
for var in binder:
    counter.append(len(data_with_binder[data_with_binder[var]>0]))
fig = go.Figure()
fig.add_trace(go.Bar(x=binder_pretty, y=counter))
fig.show()
fig = go.Figure()
for i, var in enumerate(binder):
        fig.add_trace(go.Box(y=data_with_binder.loc[data_with_binder[var]>0,var], name= binder_pretty[i],boxpoints='all'))
fig.show()

# # Missing Values

# +
# core_frame = raw_data.copy()
# raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
# fig = go.Figure()
# fig.add_trace(go.Bar(x=raw_missing.index,y=raw_missing.values, text='pop',marker_color='crimson'))
# fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
# fig.update_layout(title_text='Percentage of missing Values in all data')
# fig.show()
# -

core_frame = data_boehmite.copy()
raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
fig = go.Figure()
fig.add_trace(go.Bar(x=raw_missing.index,y=raw_missing.values, text='pop',marker_color='crimson'))
fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
fig.update_layout(title_text='Percentage of missing Values')
fig.show()

# ## Missing Values for Flow / Powder Properties

# ## Data for pure Boehmite and Boehmite with Binder

core_frame = data_boehmite.copy()
raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
raw_missing_frame = raw_missing.to_frame()
raw_missing_frame = raw_missing_frame.rename(columns={0: "Missing Values"})
raw_missing_frame = raw_missing_frame.T
filter_col = [col for col in raw_missing_frame if col.startswith('Flow')]
#raw_missing_frame = raw_missing_frame[filter_col]
raw_missing_frame = raw_missing_frame.T
raw_missing_frame_display = raw_missing_frame.style.format({'Missing Values': "{:.0%}"})
display(raw_missing_frame_display)
fig = go.Figure()
fig.add_trace(go.Bar(x=raw_missing_frame.index,y=raw_missing_frame['Missing Values'], text='pop',marker_color='crimson'))
fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
fig.update_layout(title_text='Percentage of missing Values for Flow properties')
fig.show()
list_high_missing_values = list(raw_missing_frame[raw_missing_frame['Missing Values']>0.2].index)

# ## Data for Boehmite with Binder

core_frame = data_with_binder.copy()
raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
raw_missing_frame = raw_missing.to_frame()
raw_missing_frame = raw_missing_frame.rename(columns={0: "Missing Values"})
raw_missing_frame = raw_missing_frame.T
filter_col = [col for col in raw_missing_frame if col.startswith('Flow')]
raw_missing_frame = raw_missing_frame[filter_col]
raw_missing_frame = raw_missing_frame.T
raw_missing_frame_display = raw_missing_frame.style.format({'Missing Values': "{:.0%}"})
display(raw_missing_frame_display)
fig = go.Figure()
fig.add_trace(go.Bar(x=raw_missing_frame.index,y=raw_missing_frame['Missing Values'], text='pop',marker_color='crimson'))
fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
fig.update_layout(title_text='Percentage of missing Values for Flow properties')
fig.show()

# ## Missing Values for tabletted Samples

core_frame = data_boehmite[data_boehmite['is_tabletted']=='Yes'].copy()
raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
raw_missing_frame = raw_missing.to_frame()
raw_missing_frame = raw_missing_frame.rename(columns={0: "Missing Values"})
raw_missing_frame = raw_missing_frame.T
filter_col = [col for col in raw_missing_frame if col.startswith('tablet')]
raw_missing_frame = raw_missing_frame[filter_col]
raw_missing_frame = raw_missing_frame.T
raw_missing_frame_display = raw_missing_frame.style.format({'Missing Values': "{:.0%}"})
display(raw_missing_frame_display)
fig = go.Figure()
fig.add_trace(go.Bar(x=raw_missing_frame.index,y=raw_missing_frame['Missing Values'], text='pop',marker_color='crimson'))
fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
fig.update_layout(title_text='Percentage of missing Values for tablet properties')
fig.show()

# ## Scatterplot Matrix

width ='50%'
source_frame = data_boehmite.copy()
@interact
def show_articles_more_than(parameters=widgets.SelectMultiple(options=source_frame.select_dtypes('number').columns,description='Select Parameters:', rows=15,layout=widgets.Layout(width=width))
                           ):
    index_vals = source_frame['Type'].astype('category').cat.codes
    input_var = list(parameters)
    dimensions = []
    for var in input_var:
        dimensions.append(dict(label=var,
                                 values=source_frame[var]))
    fig = go.Figure(data=go.Splom(
                dimensions=dimensions,
                diagonal_visible=True, # remove plots on diagonal
                text=source_frame['Type'],
                marker=dict(color=index_vals,
                              size=5,
                              colorscale='Bluered',
                            showscale=False, # colors encode categorical variables
                            line_color='white', line_width=0.5)
                ))

    fig.show()
