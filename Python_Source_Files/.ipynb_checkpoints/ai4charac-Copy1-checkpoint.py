# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.5.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
# story
# what values are missing?
# do some values have different names but are the same?
# try to model flow parameters from pure parameters and addition of binder
# try to model tablet properties

# technical to do:
# replace and nan handling
# -

import pandas as pd
import plotly.graph_objects as go
import ipywidgets as widgets
from ipywidgets import interact, interact_manual
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
import numpy as np
import os
import math
import feature_selection

path_to_raw_files = os.path.join(os.path.expanduser("~")+os.sep,"Clariant International Ltd\Digital methods in R&D and Engineering Services - Pilots","AI 4 Material Characterization","Alumina - predict tabletability")

#raw_data_02 = pd.read_excel(os.path.join(path_to_raw_files,'AI_4_Material_Alumina_consolidated_data.xlsx'))
raw_data = pd.read_excel('AI_4_Material_Alumina_consolidated_data.xlsx')

# +
object_columns = list(raw_data.select_dtypes('object').columns)
raw_data['NO3_content'] = raw_data['NO3_content'].replace('<0.001',np.nan)
raw_data['Cl_content'] = raw_data['Cl_content'].replace('<100',np.nan)
raw_data['Sulfur_content'] = raw_data['Sulfur_content'].replace('<10',np.nan)
raw_data['Sulfur_content'] = raw_data['Sulfur_content'].replace('<1.0',np.nan)
raw_data['Acidity_total'] = raw_data['Acidity_total'].replace('organics ',np.nan)
raw_data['tablet_tab_compression_force'] = raw_data['tablet_tab_compression_force'].replace('Not Tabbed',np.nan)
raw_data['NO3_content'].astype('float32')
raw_data['Cl_content'].astype('float32')
raw_data['Sulfur_content'].astype('float32')
raw_data['Acidity_total'].astype('float32')
raw_data['tablet_tab_compression_force'].astype('float32');

# for var in object_columns:
#     print(var)
#     display(raw_data.loc[pd.to_numeric(raw_data[var], errors='coerce').isnull(),var].unique())
# -

raw_data = raw_data.drop(['Flow_BFE_02'],axis=1)

raw_data['tablet_avg_mass'] = raw_data[['tablet_avg_mass','tablet_filling_mass']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_filling_mass'],axis=1)
raw_data['tablet_mass_dev'] = raw_data[['tablet_mass_dev','tablet_filling_mass_dev']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_filling_mass_dev'],axis=1)
raw_data['tablet_length'] = raw_data[['tablet_length','tablet_L']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_L'],axis=1)
raw_data['tablet_length_dev'] = raw_data[['tablet_length_dev','tablet_L_dev']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_L_dev'],axis=1)
raw_data['tablet_tab_ejection_force'] = raw_data[['tablet_tab_ejection_force','tablet_ejection_avg']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_ejection_avg'],axis=1)
raw_data['tablet_tab_compression_force'] = raw_data[['tablet_tab_compression_force','tablet_compression_average_14punches']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_compression_average_14punches'],axis=1)
raw_data['tablet_attrition'] = raw_data[['tablet_attrition','tablet_attrition_02']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_attrition_02'],axis=1)
raw_data['tablet_crush_strength'] = raw_data[['tablet_crush_strength','tablet_crush']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_crush'],axis=1) 

raw_data = raw_data.drop(list(raw_data.filter(regex="dev_ave").columns),axis=1)

raw_data['is_tabletted'] = 'No'
raw_data.loc[raw_data.filter(like='tablet_',axis=1).dropna(how='all').index,'is_tabletted'] = 'Yes'

flow_col = [col for col in raw_data.columns if 'Flow' in col]
flow_col = flow_col + [col for col in raw_data.columns if 'PSD' in col]
flow_col = flow_col + [col for col in raw_data.columns if 'Acidity' in col]
flow_col = flow_col + [col for col in raw_data.columns if '_content' in col]
flow_col = flow_col + [col for col in raw_data.columns if 'BET' in col]
for var in raw_data['Boehmite'].unique():
    if var not in ['None']:
        for column_name in flow_col:
            #raw_data['Pure_'+column_name] = 0
            raw_data.loc[raw_data['Boehmite']==var,'Pure_'+column_name] = raw_data.loc[(raw_data['Boehmite']==var) & (raw_data['Type']=='pure_boehmite'),column_name].values[0]  

data_boehmite = raw_data[(raw_data['Type']=='pure_boehmite') |(raw_data['Type']=='boehmite_with_binder') ]


flow_col = [col for col in data_boehmite.columns if 'Flow' in col]
flow_col = flow_col + [col for col in data_boehmite.columns if 'PSD' in col]
flow_col = flow_col + [col for col in data_boehmite.columns if 'Acidity' in col]
flow_col = flow_col + [col for col in data_boehmite.columns if '_content' in col]
flow_col = flow_col + [col for col in data_boehmite.columns if 'BET' in col]
for var in data_boehmite['Boehmite'].unique():
    for column_name in flow_col:
        #data_boehmite['Pure_'+column_name] = 0
        data_boehmite.loc[data_boehmite['Boehmite']==var,'Pure_'+column_name] = data_boehmite.loc[(data_boehmite['Boehmite']==var) & (data_boehmite['Type']=='pure_boehmite'),column_name].values[0]  

pure_boehmite = raw_data[raw_data['Type']=='pure_boehmite']
data_with_binder = data_boehmite[(data_boehmite['Type']=='boehmite_with_binder') ]

# # General

# * 5 types of samples: Pure Boehmite, Boehmite with Binder, Binder (e.g. graphite), specific sieve fraction of Boehmite with Binder, undefined (30 clicks, Claco Bayerite)
# * Some samples are tabletted

# # Statistics

count_frame = raw_data.groupby(['Type']).size().sort_values(ascending=False)
fig = go.Figure()
fig.add_trace(go.Bar(x=count_frame.index,y=count_frame.values))
fig.update_layout(title_text='Counts for each type of samples')
fig.show()

count_frame = raw_data.groupby(['is_tabletted']).size().sort_values(ascending=False)
fig = go.Figure()
fig.add_trace(go.Bar(x=count_frame.index,y=count_frame.values))
fig.update_layout(title_text='Counts: Was the sample tabletted?')
fig.show()

# ## Distribution of used binders

binder = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','WDB','SB']
binder_pretty = ['Al Stearate','Mg Stearate','Graphite','Perlite','Gurh','WDB','SB']
counter = []
for var in binder:
    counter.append(len(data_with_binder[data_with_binder[var]>0]))
fig = go.Figure()
fig.add_trace(go.Bar(x=binder_pretty, y=counter))
fig.show()
fig = go.Figure()
for i, var in enumerate(binder):
        fig.add_trace(go.Box(y=data_with_binder.loc[data_with_binder[var]>0,var], name= binder_pretty[i],boxpoints='all'))
fig.show()

# # Missing Values

# +
# core_frame = raw_data.copy()
# raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
# fig = go.Figure()
# fig.add_trace(go.Bar(x=raw_missing.index,y=raw_missing.values, text='pop',marker_color='crimson'))
# fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
# fig.update_layout(title_text='Percentage of missing Values in all data')
# fig.show()
# -

core_frame = data_boehmite.copy()
raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
fig = go.Figure()
fig.add_trace(go.Bar(x=raw_missing.index,y=raw_missing.values, text='pop',marker_color='crimson'))
fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
fig.update_layout(title_text='Percentage of missing Values')
fig.show()

# ## Missing Values for Flow / Powder Properties

# ## Data for pure Boehmite and Boehmite with Binder

core_frame = data_boehmite.copy()
raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
raw_missing_frame = raw_missing.to_frame()
raw_missing_frame = raw_missing_frame.rename(columns={0: "Missing Values"})
raw_missing_frame = raw_missing_frame.T
filter_col = [col for col in raw_missing_frame if col.startswith('Flow')]
#raw_missing_frame = raw_missing_frame[filter_col]
raw_missing_frame = raw_missing_frame.T
raw_missing_frame_display = raw_missing_frame.style.format({'Missing Values': "{:.0%}"})
display(raw_missing_frame_display)
fig = go.Figure()
fig.add_trace(go.Bar(x=raw_missing_frame.index,y=raw_missing_frame['Missing Values'], text='pop',marker_color='crimson'))
fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
fig.update_layout(title_text='Percentage of missing Values for Flow properties')
fig.show()
list_high_missing_values = list(raw_missing_frame[raw_missing_frame['Missing Values']>0.2].index)

# ## Data for Boehmite with Binder

core_frame = data_with_binder.copy()
raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
raw_missing_frame = raw_missing.to_frame()
raw_missing_frame = raw_missing_frame.rename(columns={0: "Missing Values"})
raw_missing_frame = raw_missing_frame.T
filter_col = [col for col in raw_missing_frame if col.startswith('Flow')]
raw_missing_frame = raw_missing_frame[filter_col]
raw_missing_frame = raw_missing_frame.T
raw_missing_frame_display = raw_missing_frame.style.format({'Missing Values': "{:.0%}"})
display(raw_missing_frame_display)
fig = go.Figure()
fig.add_trace(go.Bar(x=raw_missing_frame.index,y=raw_missing_frame['Missing Values'], text='pop',marker_color='crimson'))
fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
fig.update_layout(title_text='Percentage of missing Values for Flow properties')
fig.show()

# ## Missing Values for tabletted Samples

core_frame = data_boehmite[data_boehmite['is_tabletted']=='Yes'].copy()
raw_missing = core_frame.isna().sum().sort_values(ascending=False)/len(core_frame)
raw_missing_frame = raw_missing.to_frame()
raw_missing_frame = raw_missing_frame.rename(columns={0: "Missing Values"})
raw_missing_frame = raw_missing_frame.T
filter_col = [col for col in raw_missing_frame if col.startswith('tablet')]
raw_missing_frame = raw_missing_frame[filter_col]
raw_missing_frame = raw_missing_frame.T
raw_missing_frame_display = raw_missing_frame.style.format({'Missing Values': "{:.0%}"})
display(raw_missing_frame_display)
fig = go.Figure()
fig.add_trace(go.Bar(x=raw_missing_frame.index,y=raw_missing_frame['Missing Values'], text='pop',marker_color='crimson'))
fig.update_layout(barmode='stack', xaxis={'tickmode':'linear'}, yaxis={'tickformat':',.0%'}, height=800)
fig.update_layout(title_text='Percentage of missing Values for tablet properties')
fig.show()

# ## Scatterplot Matrix

width ='50%'
source_frame = data_boehmite.copy()
@interact
def show_articles_more_than(parameters=widgets.SelectMultiple(options=source_frame.select_dtypes('number').columns,description='Select Parameters:', rows=15,layout=widgets.Layout(width=width))
                           ):
    index_vals = source_frame['Type'].astype('category').cat.codes
    input_var = list(parameters)
    dimensions = []
    for var in input_var:
        dimensions.append(dict(label=var,
                                 values=source_frame[var]))
    fig = go.Figure(data=go.Splom(
                dimensions=dimensions,
                diagonal_visible=True, # remove plots on diagonal
                text=source_frame['Type'],
                marker=dict(color=index_vals,
                              size=5,
                              colorscale='Bluered',
                            showscale=False, # colors encode categorical variables
                            line_color='white', line_width=0.5)
                ))

    fig.show()


# +
# width ='50%'
# @interact
# def show_articles_more_than(inputs=widgets.SelectMultiple(options=list(data_boehmite.columns),description='Input Variables:', rows=15,layout=widgets.Layout(width=width))
#                            ):

#     input_var = list(inputs)
#     data_nona = data_boehmite.copy()
#     X = data_nona[input_var]
#     with pd.option_context("display.max_rows", None):
#         display(X)
# -

# # Modelling

# +
# width ='50%'
# source_frame = data_boehmite.copy()
# @interact
# def show_articles_more_than(target=widgets.Dropdown(options=source_frame.select_dtypes('number').columns,description='Target Variable:', rows=15,layout=widgets.Layout(width=width)),
#                             inputs=widgets.SelectMultiple(options=source_frame.select_dtypes('number').columns,description='Input Variables:', rows=15,layout=widgets.Layout(width=width))
#                            ):

#     input_var = list(inputs)
#     target = target
#     print('Target Variable: ',target)
#     input_string = []
#     for i, var in enumerate(input_var):
#         input_string += input_var[i]
#     print('Input Variables: ', ' '.join(input_var))
#     data_nona = source_frame.dropna(subset=input_var+[target])
#     print('Ratio of deleted samples due to missing values: ','{0:.0%}'.format((len(source_frame)-len(data_nona))/len(source_frame)))
# #     print(len(data_nona))
# #     print(len(source_frame))
#     X = data_nona[input_var]
#     y = data_nona[target]
#     reg = LinearRegression(fit_intercept=True).fit(X, y)
#     y_predict = reg.predict(X).reshape(1,len(y))[0]
#     y_true = y.values.reshape(1,len(y))[0]
#     ols_formula = target + ' = %0.2f +' % reg.intercept_
#     for i, col in enumerate(X.columns[0:]):
#         if np.sign(reg.coef_[i])>=0:
#             ols_formula += ' %0.2f * %s +' % (reg.coef_[i], col)
#         if np.sign(reg.coef_[i])<0:
#             ols_formula += ' %0.2f * %s -' % (abs(reg.coef_[i]), col)
#     print(' '.join(ols_formula.split(' ')[:-1]))
#     print('MSE: ', round(mean_squared_error(y_true, y_predict),2))
#     min_value = min(min(y_true),min(y_predict),0)
#     max_value = max(max(y_true),max(y_predict))
#     fig = go.Figure()
#     fig.add_trace(go.Scatter(x=y_true,y=y_predict,mode='markers',hovertext=data_nona['Sample_Name'], showlegend=False))
#     fig.add_trace(go.Scatter(x=[min_value,max_value],y=[min_value,max_value],mode='lines',hovertext=data_nona['Sample_Name'],showlegend=False))
#     fig.update_layout(title_text='Simple linear model for prediction of '+target)
#     fig.update_layout(xaxis={'title':'Measured Values of '+target}, yaxis={'title':'Predicted Values of '+target}, height=800)
#     fig.update_xaxes(range=[min_value,max_value])
#     fig.update_yaxes(range=[min_value,max_value])
#     #fig.add_trace(go.Scatter(x=[0,3000],y=[0,3000],mode='lines'))
#     fig.show()
# -

width ='50%'
source_frame = data_boehmite.copy()
target_var = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values]
@interact
def show_articles_more_than(target=widgets.Dropdown(options=source_frame.select_dtypes('number').columns, description='Target Variable:', rows=15, layout=widgets.Layout(width=width)),
                            inputs=widgets.SelectMultiple(options=target_var, description='Input Variables:', rows=15, layout=widgets.Layout(width=width))
                           ):

    input_var = list(inputs)
    target = target
    print('Target Variable: ',target)
    input_string = []
    for i, var in enumerate(input_var):
        input_string += input_var[i]
    print('Input Variables: ', ' '.join(input_var))
    data_nona = source_frame.dropna(subset=input_var+[target])
    print('Ratio of deleted samples due to missing values: ','{0:.0%}'.format((len(source_frame)-len(data_nona))/len(source_frame)))
#     print(len(data_nona))
#     print(len(source_frame))
    X = data_nona[input_var]
    y = data_nona[target]
    reg = LinearRegression(fit_intercept=False).fit(X, y)
    y_predict = reg.predict(X).reshape(1,len(y))[0]
    y_true = y.values.reshape(1,len(y))[0]
    data_nona['y_predicted'] = y_predict
    ols_formula = target + ' = %0.2f +' % reg.intercept_
    for i, col in enumerate(X.columns[0:]):
        if np.sign(reg.coef_[i])>=0:
            ols_formula += ' %0.2f * %s +' % (reg.coef_[i], col)
        if np.sign(reg.coef_[i])<0:
            ols_formula += ' %0.2f * %s -' % (abs(reg.coef_[i]), col)
    print(' '.join(ols_formula.split(' ')[:-1]))
    print('MSE: ', round(mean_squared_error(y_true, y_predict),2))
    display(data_nona.loc[(data_nona['Sample_Name']=='Pural SB') |(data_nona['Sample_Name']=='Versal V250') |(data_nona['Sample_Name']=='V250 3 Al stearate%') | (data_nona['Sample_Name']=='versal250 3% graphite') ,['Sample_Name','Flow_FRI','y_predicted']+input_var])
    min_value = min(min(y_true),min(y_predict),0)
    max_value = max(max(y_true),max(y_predict))
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=y_true,y=y_predict,mode='markers',hovertext=data_nona['Sample_Name'], showlegend=False))
    fig.add_trace(go.Scatter(x=[min_value,max_value],y=[min_value,max_value],mode='lines',hovertext=data_nona['Sample_Name'],showlegend=False))
    fig.update_layout(title_text='Simple linear model for prediction of '+target)
    fig.update_layout(xaxis={'title':'Measured Values of '+target}, yaxis={'title':'Predicted Values of '+target}, height=800)
    fig.update_xaxes(range=[min_value,max_value])
    fig.update_yaxes(range=[min_value,max_value])
    #fig.add_trace(go.Scatter(x=[0,3000],y=[0,3000],mode='lines'))
    fig.show()
    #


source_frame = data_boehmite.copy()
target_var = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and var not in ['Flow_FRI']]


def eval_func(featureset ):
    source_frame = data_boehmite.copy()
    #pd.set_option('mode.chained_assignment', None)
    target = 'Flow_FRI'
    input_var = list(featureset )
    target = target
    #print('Target Variable: ',target)
    input_string = []
    for i, var in enumerate(input_var):
        input_string += input_var[i]
    #print('Input Variables: ', ' '.join(input_var))
    data_nona = source_frame.dropna(subset=input_var+[target]).copy()
    #print('Ratio of deleted samples due to missing values: ','{0:.0%}'.format((len(source_frame)-len(data_nona))/len(source_frame)))
#     print(len(data_nona))
#     print(len(source_frame))
    X = data_nona[input_var]
    y = data_nona[target]
    reg = LinearRegression(fit_intercept=False).fit(X, y)
    y_predict = reg.predict(X).reshape(1,len(y))[0]
    y_true = y.values.reshape(1,len(y))[0]
    data_nona['y_predicted'] = y_predict
    ols_formula = target + ' = %0.2f +' % reg.intercept_
    for i, col in enumerate(X.columns[0:]):
        if np.sign(reg.coef_[i])>=0:
            ols_formula += ' %0.2f * %s +' % (reg.coef_[i], col)
        if np.sign(reg.coef_[i])<0:
            ols_formula += ' %0.2f * %s -' % (abs(reg.coef_[i]), col)
    #print(' '.join(ols_formula.split(' ')[:-1]))
    #print('MSE: ', round(mean_squared_error(y_true, y_predict),2))
    return mean_squared_error(y_true, y_predict)


must_feature = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','WDB','SB','Pure_Flow_FRI']
can_features = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and var not in ['Flow_FRI']]
can_features = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and 'Pure' in var]
print(len(target_var))
print(len(can_features))
forward_selector_func = feature_selection.FeatureSelector()
forward_selector_func.forward_selector(must_features=must_feature,can_features=can_features,evaluationfunction=eval_func, better='lower',max_features=len(must_feature)+4)

forward_selector_func.best_set()

# # Backup

input_var = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','MN2','WDB','SB','Pure_BFE']
target = ['Flow_BFE']
#data_nona = data_boehmite[data_boehmite['Boehmite']=='Versal_V700'].dropna(subset=target)
data_nona = data_boehmite.dropna(subset=target)
X = data_nona[input_var]
y = data_nona[target]
regressor = DecisionTreeRegressor(random_state=0, max_depth=3)
regressor.fit(X, y)
y_predict = regressor.predict(X).reshape(1,len(y))[0]
y_true = y.values.reshape(1,len(y))[0]
fig = go.Figure()
fig.add_trace(go.Scatter(x=y_true,y=y_predict,mode='markers',hovertext=data_nona['Sample_Name']))
fig.add_trace(go.Scatter(x=[0,3000],y=[0,3000],mode='lines'))
fig.show()


@interact
def show_articles_more_than(column=widgets.Dropdown(options=pure_boehmite.columns), 
                           ):
    #plot_df = pure_boehmite.dropna(subset=[column])
    fig = go.Figure()
    fig.add_trace(go.Box(y=pure_boehmite[column], name=column,hovertext=pure_boehmite['Sample_Name'], boxpoints='all'))

    fig.show()


@interact
def show_articles_more_than(column=widgets.Dropdown(options=pure_boehmite.columns), 
                           ):
    #plot_df = pure_boehmite.dropna(subset=[column])
    fig = go.Figure()
    fig.add_trace(go.Bar(y=pure_boehmite[column],x=pure_boehmite['Sample_Name'], name=column,hovertext=pure_boehmite['Sample_Name']))

    fig.show()
@interact
def show_articles_more_than(column=widgets.Dropdown(options=raw_data.columns), 
                           ):
    fig = go.Figure()
    fig.add_trace(go.Bar(y=raw_data[column],x=raw_data['Sample_Name'], name=column,hovertext=raw_data['Sample_Name']))
    fig.show()
fig = go.Figure()
fig.add_trace(go.Bar(y=raw_data.isna().sum()/raw_data.shape[0],x=raw_data.columns))
fig.show()






