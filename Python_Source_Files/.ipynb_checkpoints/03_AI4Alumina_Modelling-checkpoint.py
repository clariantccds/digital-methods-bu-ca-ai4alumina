# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.5.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
import plotly.graph_objects as go
import ipywidgets as widgets
from ipywidgets import interact, interact_manual
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
import numpy as np
import os
import math
import feature_selection

path_to_raw_files = os.path.join(os.path.expanduser("~")+os.sep,"Clariant International Ltd\Digital methods in R&D and Engineering Services - Pilots","AI 4 Material Characterization","Alumina - predict tabletability")

#raw_data_02 = pd.read_excel(os.path.join(path_to_raw_files,'AI_4_Material_Alumina_consolidated_data.xlsx'))
raw_data = pd.read_excel('AI_4_Alumina_Prepared_Data.xlsx')

data_boehmite = raw_data[(raw_data['Type']=='pure_boehmite') |(raw_data['Type']=='boehmite_with_binder') ]
pure_boehmite = raw_data[raw_data['Type']=='pure_boehmite']
data_with_binder = data_boehmite[(data_boehmite['Type']=='boehmite_with_binder') ]

width ='50%'
source_frame = data_boehmite.copy()
target_var = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values]
@interact
def show_articles_more_than(target=widgets.Dropdown(options=source_frame.select_dtypes('number').columns, description='Target Variable:', rows=15, layout=widgets.Layout(width=width)),
                            inputs=widgets.SelectMultiple(options=target_var, description='Input Variables:', rows=15, layout=widgets.Layout(width=width))
                           ):

    input_var = list(inputs)
    target = target
    print('Target Variable: ',target)
    input_string = []
    for i, var in enumerate(input_var):
        input_string += input_var[i]
    print('Input Variables: ', ' '.join(input_var))
    data_nona = source_frame.dropna(subset=input_var+[target])
    print('Ratio of deleted samples due to missing values: ','{0:.0%}'.format((len(source_frame)-len(data_nona))/len(source_frame)))
#     print(len(data_nona))
#     print(len(source_frame))
    X = data_nona[input_var]
    y = data_nona[target]
    reg = LinearRegression(fit_intercept=False).fit(X, y)
    y_predict = reg.predict(X).reshape(1,len(y))[0]
    y_true = y.values.reshape(1,len(y))[0]
    data_nona['y_predicted'] = y_predict
    ols_formula = target + ' = %0.2f +' % reg.intercept_
    for i, col in enumerate(X.columns[0:]):
        if np.sign(reg.coef_[i])>=0:
            ols_formula += ' %0.2f * %s +' % (reg.coef_[i], col)
        if np.sign(reg.coef_[i])<0:
            ols_formula += ' %0.2f * %s -' % (abs(reg.coef_[i]), col)
    print(' '.join(ols_formula.split(' ')[:-1]))
    print('MSE: ', round(mean_squared_error(y_true, y_predict),2))
    display(data_nona.loc[(data_nona['Sample_Name']=='Pural SB') |(data_nona['Sample_Name']=='Versal V250') |(data_nona['Sample_Name']=='V250 3 Al stearate%') | (data_nona['Sample_Name']=='versal250 3% graphite') ,['Sample_Name','Flow_FRI','y_predicted']+input_var])
    min_value = min(min(y_true),min(y_predict),0)
    max_value = max(max(y_true),max(y_predict))
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=y_true,y=y_predict,mode='markers',hovertext=data_nona['Sample_Name'], showlegend=False))
    fig.add_trace(go.Scatter(x=[min_value,max_value],y=[min_value,max_value],mode='lines',hovertext=data_nona['Sample_Name'],showlegend=False))
    fig.update_layout(title_text='Simple linear model for prediction of '+target)
    fig.update_layout(xaxis={'title':'Measured Values of '+target}, yaxis={'title':'Predicted Values of '+target}, height=800)
    fig.update_xaxes(range=[min_value,max_value])
    fig.update_yaxes(range=[min_value,max_value])
    #fig.add_trace(go.Scatter(x=[0,3000],y=[0,3000],mode='lines'))
    fig.show()
    #


source_frame = data_boehmite.copy()
target_var = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and var not in ['Flow_FRI']]


def eval_func(featureset ):
    source_frame = data_boehmite.copy()
    #pd.set_option('mode.chained_assignment', None)
    target = 'Flow_FRI'
    input_var = list(featureset )
    target = target
    #print('Target Variable: ',target)
    input_string = []
    for i, var in enumerate(input_var):
        input_string += input_var[i]
    #print('Input Variables: ', ' '.join(input_var))
    data_nona = source_frame.dropna(subset=input_var+[target]).copy()
    #print('Ratio of deleted samples due to missing values: ','{0:.0%}'.format((len(source_frame)-len(data_nona))/len(source_frame)))
#     print(len(data_nona))
#     print(len(source_frame))
    X = data_nona[input_var]
    y = data_nona[target]
    reg = LinearRegression(fit_intercept=False).fit(X, y)
    y_predict = reg.predict(X).reshape(1,len(y))[0]
    y_true = y.values.reshape(1,len(y))[0]
    data_nona['y_predicted'] = y_predict
    ols_formula = target + ' = %0.2f +' % reg.intercept_
    for i, col in enumerate(X.columns[0:]):
        if np.sign(reg.coef_[i])>=0:
            ols_formula += ' %0.2f * %s +' % (reg.coef_[i], col)
        if np.sign(reg.coef_[i])<0:
            ols_formula += ' %0.2f * %s -' % (abs(reg.coef_[i]), col)
    #print(' '.join(ols_formula.split(' ')[:-1]))
    #print('MSE: ', round(mean_squared_error(y_true, y_predict),2))
    return mean_squared_error(y_true, y_predict)


must_feature = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','WDB','SB','Pure_Flow_FRI']
can_features = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and var not in ['Flow_FRI']]
can_features = [var for var in list(source_frame.select_dtypes('number').columns) if var not in list_high_missing_values and 'Pure' in var]
print(len(target_var))
print(len(can_features))
forward_selector_func = feature_selection.FeatureSelector()
forward_selector_func.forward_selector(must_features=must_feature,can_features=can_features,evaluationfunction=eval_func, better='lower',max_features=len(must_feature)+4)

forward_selector_func.best_set()

# # Backup

input_var = ['Al_stearate','Mg_Stearate','graphite','perlite','Gurh','MN2','WDB','SB','Pure_BFE']
target = ['Flow_BFE']
#data_nona = data_boehmite[data_boehmite['Boehmite']=='Versal_V700'].dropna(subset=target)
data_nona = data_boehmite.dropna(subset=target)
X = data_nona[input_var]
y = data_nona[target]
regressor = DecisionTreeRegressor(random_state=0, max_depth=3)
regressor.fit(X, y)
y_predict = regressor.predict(X).reshape(1,len(y))[0]
y_true = y.values.reshape(1,len(y))[0]
fig = go.Figure()
fig.add_trace(go.Scatter(x=y_true,y=y_predict,mode='markers',hovertext=data_nona['Sample_Name']))
fig.add_trace(go.Scatter(x=[0,3000],y=[0,3000],mode='lines'))
fig.show()


@interact
def show_articles_more_than(column=widgets.Dropdown(options=pure_boehmite.columns), 
                           ):
    #plot_df = pure_boehmite.dropna(subset=[column])
    fig = go.Figure()
    fig.add_trace(go.Box(y=pure_boehmite[column], name=column,hovertext=pure_boehmite['Sample_Name'], boxpoints='all'))

    fig.show()


@interact
def show_articles_more_than(column=widgets.Dropdown(options=pure_boehmite.columns), 
                           ):
    #plot_df = pure_boehmite.dropna(subset=[column])
    fig = go.Figure()
    fig.add_trace(go.Bar(y=pure_boehmite[column],x=pure_boehmite['Sample_Name'], name=column,hovertext=pure_boehmite['Sample_Name']))

    fig.show()
@interact
def show_articles_more_than(column=widgets.Dropdown(options=raw_data.columns), 
                           ):
    fig = go.Figure()
    fig.add_trace(go.Bar(y=raw_data[column],x=raw_data['Sample_Name'], name=column,hovertext=raw_data['Sample_Name']))
    fig.show()
fig = go.Figure()
fig.add_trace(go.Bar(y=raw_data.isna().sum()/raw_data.shape[0],x=raw_data.columns))
fig.show()






