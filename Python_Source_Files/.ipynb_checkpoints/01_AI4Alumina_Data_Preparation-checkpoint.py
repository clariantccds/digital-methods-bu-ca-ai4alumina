# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.5.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
import plotly.graph_objects as go
import ipywidgets as widgets
from ipywidgets import interact, interact_manual
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
import numpy as np
import os
import math
import feature_selection

path_to_raw_files = os.path.join(os.path.expanduser("~")+os.sep,"Clariant International Ltd\Digital methods in R&D and Engineering Services - Pilots","AI 4 Material Characterization","Alumina - predict tabletability")

#raw_data_02 = pd.read_excel(os.path.join(path_to_raw_files,'AI_4_Material_Alumina_consolidated_data.xlsx'))
raw_data = pd.read_excel('AI_4_Material_Alumina_consolidated_data.xlsx')

object_columns = list(raw_data.select_dtypes('object').columns)
raw_data['NO3_content'] = raw_data['NO3_content'].replace('<0.001',np.nan)
raw_data['Cl_content'] = raw_data['Cl_content'].replace('<100',np.nan)
raw_data['Sulfur_content'] = raw_data['Sulfur_content'].replace('<10',np.nan)
raw_data['Sulfur_content'] = raw_data['Sulfur_content'].replace('<1.0',np.nan)
raw_data['Acidity_total'] = raw_data['Acidity_total'].replace('organics ',np.nan)
raw_data['tablet_tab_compression_force'] = raw_data['tablet_tab_compression_force'].replace('Not Tabbed',np.nan)
raw_data['NO3_content'].astype('float32')
raw_data['Cl_content'].astype('float32')
raw_data['Sulfur_content'].astype('float32')
raw_data['Acidity_total'].astype('float32')
raw_data['tablet_tab_compression_force'].astype('float32');

raw_data = raw_data.drop(['Flow_BFE_02'],axis=1)

raw_data['tablet_avg_mass'] = raw_data[['tablet_avg_mass','tablet_filling_mass']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_filling_mass'],axis=1)
raw_data['tablet_mass_dev'] = raw_data[['tablet_mass_dev','tablet_filling_mass_dev']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_filling_mass_dev'],axis=1)
raw_data['tablet_length'] = raw_data[['tablet_length','tablet_L']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_L'],axis=1)
raw_data['tablet_length_dev'] = raw_data[['tablet_length_dev','tablet_L_dev']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_L_dev'],axis=1)
raw_data['tablet_tab_ejection_force'] = raw_data[['tablet_tab_ejection_force','tablet_ejection_avg']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_ejection_avg'],axis=1)
raw_data['tablet_tab_compression_force'] = raw_data[['tablet_tab_compression_force','tablet_compression_average_14punches']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_compression_average_14punches'],axis=1)
raw_data['tablet_attrition'] = raw_data[['tablet_attrition','tablet_attrition_02']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_attrition_02'],axis=1)
raw_data['tablet_crush_strength'] = raw_data[['tablet_crush_strength','tablet_crush']].sum(axis=1,min_count=1)
raw_data = raw_data.drop(['tablet_crush'],axis=1) 

raw_data = raw_data.drop(list(raw_data.filter(regex="dev_ave").columns),axis=1)

raw_data['is_tabletted'] = 'No'
raw_data.loc[raw_data.filter(like='tablet_',axis=1).dropna(how='all').index,'is_tabletted'] = 'Yes'

flow_col = [col for col in raw_data.columns if 'Flow' in col]
flow_col = flow_col + [col for col in raw_data.columns if 'PSD' in col]
flow_col = flow_col + [col for col in raw_data.columns if 'Acidity' in col]
flow_col = flow_col + [col for col in raw_data.columns if '_content' in col]
flow_col = flow_col + [col for col in raw_data.columns if 'BET' in col]
for var in raw_data['Boehmite'].unique():
    if var not in ['None']:
        for column_name in flow_col:
            #raw_data['Pure_'+column_name] = 0
            raw_data.loc[raw_data['Boehmite']==var,'Pure_'+column_name] = raw_data.loc[(raw_data['Boehmite']==var) & (raw_data['Type']=='pure_boehmite'),column_name].values[0]  
