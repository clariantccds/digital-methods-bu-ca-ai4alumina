import random
import numpy as np
import pandas as pd
import matplotlib.pylab as plt

class FeatureSelector():
    '''
    Provides functionality to select features with a genetic algorithm
    '''

    def genetic_selector( self, must_features, can_features, evaluationfunction, n_generations = 10, n_individuals = 10, n_mutations = 5, group_size = 3, better = 'higher', **kwargs):


        self.all_individuals = []
        self.all_fitnesses = []
        self.can_features = can_features
        self.must_features = must_features
        #self.all_features = can_features + must_features
        self.better = better
        self.selector_used = 'Genetic'

        individuals = [ [random.randint(0,1) for i in range(self.can_features.__len__())] for j in range(n_individuals) ]
        


        for gen in range(n_generations):
            print('generation', gen)

            ### crossover
            offspring = []
            cross_individuals = random.sample(range(n_individuals),n_individuals)
            for i in range(int(n_individuals/2)):
                thisoffspring = list(individuals[cross_individuals[i]][:int(self.can_features.__len__()/2)]) + list(individuals[cross_individuals[i+1]][int(self.can_features.__len__()/2):])
                offspring.append(thisoffspring)

            individuals = individuals + offspring

            ### mutation
            for i in range(individuals.__len__()):
                for j in range(n_mutations):
                    flipfeature = random.randint(0,self.can_features.__len__()-1)
                    if (individuals[i][flipfeature] == 0):
                        individuals[i][flipfeature] = 1
                    else:
                        individuals[i][flipfeature] = 0

            ### evaluation
            fitness = [0. for i in range(individuals.__len__())]
            for i in range(individuals.__len__()):
                #fitness[i] = evaluatemodel( featureset = list(np.array(all_features)[np.array(individuals[i]).astype('bool')]), target = targetfeature )['best_testscore'][0]
                fitness[i] = evaluationfunction ( featureset = list(self.must_features) + list(np.array(self.can_features)[np.array(individuals[i]).astype('bool')]), **kwargs )
                print('evaluating individual', i, 'fitness:', fitness[i])
                print('Length of featureset: ',len(list(self.must_features) + list(np.array(self.can_features)[np.array(individuals[i]).astype('bool')])))


            self.all_individuals.append(individuals)
            self.all_fitnesses.append(fitness)

            print('average fitness during generation ' + str(gen) + ': ' + str(np.mean(fitness)))

            ### selection
            groups = [ [ random.randint(0,individuals.__len__()-1) for j in range(group_size) ] for i in range(n_individuals) ]
            respective_fitness = [[fitness[groups[i][j]] for j in range(group_size)] for i in range(n_individuals)]
            if (better == 'higher'):
                group_winners = [ np.argmax(respective_fitness[i]) for i in range(n_individuals) ]
            else:
                group_winners = [ np.argmin(respective_fitness[i]) for i in range(n_individuals) ]
            new_individuals = list(np.array(individuals)[[ groups[i][group_winners[i]] for i in range(groups.__len__()) ]])
            individuals = new_individuals

        run = self.best_individual()
        print('Done with Genetic feature selection. Best score found:', self.best_score(), 'in generation', run[0], 'run', run[1])


    def best_individual(self):
        if (self.selector_used == 'Genetic') or (self.selector_used == 'Random'):
            if (self.better == 'higher'):
                best_individual_in_generation = np.argmax([ np.max(self.all_fitnesses[i]) for i in range(self.all_fitnesses.__len__()) ])
                best_individual_run = np.argmax(self.all_fitnesses[best_individual_in_generation])
            else:
                best_individual_in_generation = np.argmin([ np.min(self.all_fitnesses[i]) for i in range(self.all_fitnesses.__len__()) ])
                best_individual_run = np.argmin(self.all_fitnesses[best_individual_in_generation])
            return [ best_individual_in_generation , best_individual_run ]
        if (self.selector_used == 'Forward') or (self.selector_used == 'Backward'):
            return self.all_individuals


    def best_score(self):
        if (self.better == 'higher'):
            return np.max(self.all_fitnesses)
        else:
            return np.min(self.all_fitnesses)


    def best_set(self):
        if (self.selector_used == 'Genetic') or (self.selector_used == 'Random'):
            [ best_gen, best_run ] = self.best_individual()
            return list(np.array(self.can_features)[np.array(self.all_individuals[best_gen][best_run]).astype(bool)])
        if (self.selector_used == 'Forward') or (self.selector_used == 'Backward'):
            return self.all_individuals


    def plot_genetic_evolution(self):
        fig,ax = plt.subplots()
        ax.plot([ np.mean(self.all_fitnesses[i]) for i in range(self.all_fitnesses.__len__()) ], label = 'mean score')
        if (self.better == 'higher'):
            ax.plot([ np.max(self.all_fitnesses[i]) for i in range(self.all_fitnesses.__len__()) ], label = 'best score')
        else:
            ax.plot([ np.min(self.all_fitnesses[i]) for i in range(self.all_fitnesses.__len__()) ], label = 'best score')
        ax.legend()

    def forward_selector( self, must_features, can_features, evaluationfunction, max_features = 10, better = 'higher', **kwargs):


        self.all_individuals = []
        self.all_fitnesses = []
        self.can_features = can_features
        self.must_features = must_features
        self.better = better
        self.selector_used = 'Forward'

        
        selected_feature_set = must_features
        winner_set = selected_feature_set
        unevaluated_set = can_features
        winner_item = ''

        if len(must_features)>0:
            temp_score = evaluationfunction(featureset = selected_feature_set, **kwargs ) 
            #print('Must Feature Score:',temp_score)  
        else:
            if better == 'lower':
                temp_score=np.inf
            if better == 'higher':
                temp_score = 0

        while len(selected_feature_set) < max_features:
            fitness = [0. for i in range(len(unevaluated_set))]
            for item_loop in range(len(unevaluated_set)):
                selected_feature_set = winner_set + [unevaluated_set[item_loop]]
                fitness[item_loop] = evaluationfunction(featureset = selected_feature_set, **kwargs )
                if better == 'lower':
                    if fitness[item_loop] <= temp_score:
                        temp_score = fitness[item_loop]                    
                        winner_item = unevaluated_set[item_loop]
                if better == 'higher':
                    if fitness[item_loop] >= temp_score:
                        temp_score = fitness[item_loop]                    
                        winner_item = unevaluated_set[item_loop]
                #print(unevaluated_set[item_loop],fitness[item_loop])
            if winner_item == '':
                #print('Found no new winner')
                break
            #print('winner:',winner_item,' with score:',temp_score)
            winner_set = winner_set + [winner_item]
            unevaluated_set.remove(winner_item)
            self.all_individuals.append(winner_item)
            self.all_fitnesses.append(fitness)
            winner_item = ''

    def backward_selector( self, must_features, can_features, evaluationfunction, max_features = 10, better = 'higher', **kwargs):


        self.all_individuals = []
        self.all_fitnesses = []
        self.can_features = can_features
        self.must_features = must_features
        self.better = better
        self.selector_used = 'Backward'
        winner_item = ''

        
        selected_feature_set = must_features+can_features
        winner_set = selected_feature_set
        unevaluated_set = can_features

        if len(selected_feature_set)>0:
            temp_score = evaluationfunction(featureset = selected_feature_set, **kwargs ) 
            print('Taking All Features Score:',temp_score)  
        else:
            if better == 'lower':
                temp_score=0
            if better == 'higher':
                temp_score = np.inf

        while len(selected_feature_set) > max_features:
            fitness = [0. for i in range(len(unevaluated_set))]
            for item_loop in range(len(unevaluated_set)):
                selected_feature_set = list(set(winner_set) - set([unevaluated_set[item_loop]]))
                #selected_feature_set = winner_set - [unevaluated_set[item_loop]]
                fitness[item_loop] = evaluationfunction(featureset = selected_feature_set, **kwargs )
                if better == 'lower':
                    if fitness[item_loop] > temp_score:
                        temp_score = fitness[item_loop]                    
                        winner_item = unevaluated_set[item_loop]
                if better == 'higher':
                    if fitness[item_loop] < temp_score:
                        temp_score = fitness[item_loop]                    
                        winner_item = unevaluated_set[item_loop]
                print(unevaluated_set[item_loop],fitness[item_loop])
            if winner_item == '':
                print('Found no new winner')
                break
            print('winner:',winner_item,' with score:',temp_score)
            winner_set = list(set(winner_set) - set([winner_item]))
            unevaluated_set.remove(winner_item)
            self.all_individuals.append(winner_item)
            self.all_fitnesses.append(fitness)
            winner_item = ''
            
    def random_selector( self, must_features, can_features, evaluationfunction, n_samples = 10, min_features = 10, max_features = 10, better = 'higher', **kwargs):


            self.all_individuals = []
            self.all_fitnesses = []
            self.can_features = can_features
            self.must_features = must_features
            self.better = better
            self.selector_used = 'Random'

            
            individuals = [ [0 for i in range(len(can_features))] for j in range(n_samples) ]
            randomset = [random.sample(population=range(len(can_features)),k=random.randint(min_features-len(must_features),max_features-len(must_features)))  for j in range(n_samples) ]
            for i in range(len(randomset)):
                for j in randomset[i]: 
                    individuals[i][j]=1


            

            ### evaluation
            fitness = [0. for i in range(individuals.__len__())]
            for i in range(individuals.__len__()):
                #fitness[i] = evaluatemodel( featureset = list(np.array(all_features)[np.array(individuals[i]).astype('bool')]), target = targetfeature )['best_testscore'][0]
                fitness[i] = evaluationfunction ( featureset = list(self.must_features) + list(np.array(self.can_features)[np.array(individuals[i]).astype('bool')]), **kwargs )
                print('evaluating individual', i, 'fitness:', fitness[i])
                print('Length of featureset: ',len(list(self.must_features) + list(np.array(self.can_features)[np.array(individuals[i]).astype('bool')])))


            self.all_individuals.append(individuals)
            self.all_fitnesses.append(fitness)

            print('average fitness during random sampling: ' + str(np.mean(fitness)))

            

            run = self.best_individual()
            print('Done with Random feature selection. Best score found:', self.best_score())
        
            
        